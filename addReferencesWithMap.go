package errorwrapper

import (
	"gitlab.com/evatix-go/errorwrapper/ref"
	"gitlab.com/evatix-go/errorwrapper/refs"
)

func addReferencesWithMap(
	references []ref.Value,
	referencesMap map[string]string,
	clonedNew *Wrapper,
) *Wrapper {
	refLength := len(references)
	if refLength == 0 && len(referencesMap) == 0 {
		return clonedNew
	}

	if clonedNew.references == nil {
		// create new
		clonedNew.references = refs.New(refLength)
	}

	if clonedNew.references != nil && len(references) > 0 {
		clonedNew.references.Adds(references...)
	}

	if clonedNew.references != nil && len(referencesMap) > 0 {
		clonedNew.references.AddHashMap(referencesMap)
	}

	return clonedNew
}
