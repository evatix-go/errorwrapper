package main

import (
	"fmt"

	"gitlab.com/evatix-go/errorwrapper"
	"gitlab.com/evatix-go/errorwrapper/errnew"
	"gitlab.com/evatix-go/errorwrapper/errwrappers"
)

func NullTest01() {
	var errWrap *errorwrapper.Wrapper
	var errWpC *errwrappers.Collection

	err1 := errnew.Null.ManyByChecking(errWpC, errWrap)

	fmt.Println(err1)

	err2 := errnew.Null.ManyByChecking(nil, errWrap)

	fmt.Println(err2)

	err3 := errnew.Null.ManyWithMessage("Something wrong", nil, errWrap)

	fmt.Println(err3)
}
