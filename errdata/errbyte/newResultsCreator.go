package errbyte

import (
	"gitlab.com/evatix-go/core/codestack"
	"gitlab.com/evatix-go/core/coredata/corejson"
	"gitlab.com/evatix-go/core/coreinterface/serializerinf"
	"gitlab.com/evatix-go/core/isany"
	"gitlab.com/evatix-go/errorwrapper"
	"gitlab.com/evatix-go/errorwrapper/errnew"
	"gitlab.com/evatix-go/errorwrapper/errtype"
)

type newResultsCreator struct{}

func (it newResultsCreator) Empty() *Results {
	return &Results{}
}

func (it newResultsCreator) UsingSafeBytesWithErrWrapGetter(
	safeBytesGetter errorwrapper.SafeBytesWithErrWrapGetter,
) *Results {
	if safeBytesGetter == nil {
		return &Results{
			ErrorWrapper: errnew.Null.Simple(safeBytesGetter),
		}
	}

	errWrap := safeBytesGetter.ErrWrap()

	if errWrap.HasError() {
		return &Results{
			ErrorWrapper: errWrap,
		}
	}

	return &Results{
		Values: safeBytesGetter.SafeValues(),
	}
}

func (it newResultsCreator) UsingAnyItem(
	errType errtype.Variation,
	anyItem interface{},
) *Results {
	if isany.Null(anyItem) {
		return &Results{
			ErrorWrapper: errnew.Null.Simple(anyItem),
		}
	}

	jsonResult := corejson.AnyTo.SerializedJsonResult(
		anyItem)

	return it.UsingJsonResult(
		errType,
		jsonResult)
}

func (it newResultsCreator) UsingSerializerFunc(
	errType errtype.Variation,
	serializerFunc func() ([]byte, error),
) *Results {
	if serializerFunc == nil {
		return &Results{
			ErrorWrapper: errnew.Null.Simple(serializerFunc),
		}
	}

	jsonResult := corejson.
		NewResult.
		UsingSerializerFunc(serializerFunc)

	return it.UsingJsonResult(
		errType,
		jsonResult)
}

func (it newResultsCreator) UsingSerializer(
	errType errtype.Variation,
	serializer serializerinf.Serializer,
) *Results {
	if serializer == nil {
		return &Results{
			ErrorWrapper: errnew.Null.Simple(serializer),
		}
	}

	jsonResult := corejson.
		NewResult.
		UsingSerializer(serializer)

	return it.UsingJsonResult(
		errType,
		jsonResult)
}

func (it newResultsCreator) UsingJsonerDefault(
	jsoner corejson.Jsoner,
) *Results {
	if jsoner == nil {
		return &Results{
			ErrorWrapper: errnew.Null.Simple(jsoner),
		}
	}

	return it.UsingJsonResultDefault(
		jsoner.JsonPtr())
}

func (it newResultsCreator) UsingJsoner(
	errType errtype.Variation,
	jsoner corejson.Jsoner,
) *Results {
	if jsoner == nil {
		return &Results{
			ErrorWrapper: errnew.Null.Simple(jsoner),
		}
	}

	return it.UsingJsonResult(
		errType,
		jsoner.JsonPtr())
}

func (it newResultsCreator) UsingJsonResult(
	errType errtype.Variation,
	jsonResult *corejson.Result,
) *Results {
	if jsonResult.HasError() {
		return &Results{
			ErrorWrapper: errnew.Type.ErrorUsingStackSkip(
				codestack.Skip1,
				errType,
				jsonResult.MeaningfulError()),
			Values: jsonResult.SafeValues(),
		}
	}

	return &Results{
		Values: jsonResult.SafeValues(),
	}
}

func (it newResultsCreator) UsingJsonResultDefault(
	jsonResult *corejson.Result,
) *Results {
	if jsonResult.HasError() {
		return &Results{
			ErrorWrapper: errnew.Type.ErrorUsingStackSkip(
				codestack.Skip1,
				errtype.Serialize,
				jsonResult.MeaningfulError()),
			Values: jsonResult.SafeValues(),
		}
	}

	return &Results{
		Values: jsonResult.SafeValues(),
	}
}

func (it newResultsCreator) All(
	values []byte,
	errType errtype.Variation,
	err error,
) *Results {
	return &Results{
		ErrorWrapper: errnew.Type.ErrorUsingStackSkip(
			codestack.Skip1,
			errType,
			err),
		Values: values,
	}
}

func (it newResultsCreator) Error(
	errType errtype.Variation,
	err error,
) *Results {
	return &Results{
		ErrorWrapper: errnew.Type.ErrorUsingStackSkip(
			codestack.Skip1,
			errType,
			err),
	}
}

func (it newResultsCreator) ErrorWrapper(
	errorWrapper *errorwrapper.Wrapper,
) *Results {
	return &Results{
		ErrorWrapper: errorWrapper,
	}
}

func (it newResultsCreator) Create(
	errWrapper *errorwrapper.Wrapper,
	values []byte,
) *Results {
	if len(values) == 0 {
		return it.ErrorWrapper(errWrapper)
	}

	return &Results{
		Values:       values,
		ErrorWrapper: errWrapper,
	}
}

func (it newResultsCreator) SpreadCreate(
	errWrapper *errorwrapper.Wrapper,
	values ...byte,
) *Results {
	if len(values) == 0 {
		return it.ErrorWrapper(errWrapper)
	}

	return &Results{
		Values:       values,
		ErrorWrapper: errWrapper,
	}
}

func (it newResultsCreator) String(
	valueString string,
) *Results {
	if len(valueString) == 0 {
		return it.Empty()
	}

	return &Results{
		Values: []byte(valueString),
	}
}

func (it newResultsCreator) StringWithErrorWrapper(
	valueString string,
	errorWrapper *errorwrapper.Wrapper,
) *Results {
	if len(valueString) == 0 {
		return it.Empty()
	}

	return &Results{
		Values:       []byte(valueString),
		ErrorWrapper: errorWrapper,
	}
}

func (it newResultsCreator) ValuesOnly(
	values []byte,
) *Results {
	if len(values) == 0 {
		return it.Empty()
	}

	return &Results{
		Values: values,
	}
}

func (it newResultsCreator) Bytes(
	rawBytes []byte,
) *Results {
	if len(rawBytes) == 0 {
		return it.Empty()
	}

	return &Results{
		Values: rawBytes,
	}
}

func (it newResultsCreator) SpreadValuesOnly(
	values ...byte,
) *Results {
	if len(values) == 0 {
		return it.Empty()
	}

	return &Results{
		Values: values,
	}
}

func (it newResultsCreator) Items(
	values []byte,
) *Results {
	if len(values) == 0 {
		return it.Empty()
	}

	return &Results{
		Values: values,
	}
}
