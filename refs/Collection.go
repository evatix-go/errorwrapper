package refs

import (
	"encoding/json"
	"fmt"
	"sort"
	"strconv"
	"strings"

	"gitlab.com/evatix-go/core/constants"
	"gitlab.com/evatix-go/core/corecsv"
	"gitlab.com/evatix-go/core/coredata/coredynamic"
	"gitlab.com/evatix-go/core/coredata/corejson"
	"gitlab.com/evatix-go/core/coredata/corepayload"
	"gitlab.com/evatix-go/core/coreimpl/enumimpl"
	"gitlab.com/evatix-go/core/coreinterface/enuminf"
	"gitlab.com/evatix-go/core/coreinterface/errcoreinf"
	"gitlab.com/evatix-go/core/coretaskinfo"
	"gitlab.com/evatix-go/core/defaulterr"
	"gitlab.com/evatix-go/core/isany"
	"gitlab.com/evatix-go/errorwrapper/ref"
)

type Collection struct {
	isRequiresSorting bool
	refs              []ref.Value
}

func (it *Collection) MapStringString() map[string]string {
	return it.DynamicMap().ConvMapStringString()
}

func (it *Collection) CloneNewDefiner() errcoreinf.ReferenceCollectionDefiner {
	return it.ClonePtr()
}

func (it *Collection) ReferencesList() []errcoreinf.Referencer {
	return it.ReferencerCollection()
}

func (it *Collection) ReferencerCollection() []errcoreinf.Referencer {
	if it.IsEmpty() {
		return []errcoreinf.Referencer{}
	}

	newSlice := make([]errcoreinf.Referencer, it.Length())

	for i, value := range it.refs {
		newSlice[i] = value.AsReferencer()
	}

	return newSlice
}

func (it *Collection) Count() int {
	return it.Length()
}

func (it *Collection) AddVarVal(
	varName string, val interface{},
) errcoreinf.ReferenceCollectionDefiner {
	it.Add(varName, val)

	return it
}

func (it *Collection) AddReferencer(
	ref errcoreinf.Referencer,
) errcoreinf.ReferenceCollectionDefiner {
	if ref == nil || it.IsEmpty() {
		return it
	}

	it.AddReferencer(ref)

	return it
}

func (it *Collection) AddReferences(
	references ...errcoreinf.Referencer,
) errcoreinf.ReferenceCollectionDefiner {
	if len(references) == 0 {
		return it
	}

	for _, reference := range references {
		if reference == nil || it.IsEmpty() {
			continue
		}

		it.AddReferencer(reference)
	}

	return it
}

func (it *Collection) MapStringAny() map[string]interface{} {
	if it.IsEmpty() {
		return map[string]interface{}{}
	}

	newMap := make(map[string]interface{}, it.Length())

	for _, value := range it.refs {
		switch casted := value.Value.(type) {
		case []byte:
			newMap[value.KeyName()] = corejson.BytesToString(
				casted)

			continue
		case []string:
			newMap[value.KeyName()] = corecsv.StringsToStringDefault(
				casted...)

			continue
		}

		newMap[value.KeyName()] = value.Value
	}

	return newMap
}

func (it *Collection) Serialize() ([]byte, error) {
	return corejson.Serialize.Raw(it)
}

func (it *Collection) SerializeMust() (jsonBytes []byte) {
	return it.JsonPtr().RawMust()
}

func (it *Collection) Compile() string {
	return it.String()
}

func (it *Collection) ReflectSetTo(
	toPointer interface{},
) error {
	return coredynamic.ReflectSetFromTo(it, toPointer)
}

func (it *Collection) Add(
	name string,
	val interface{},
) *Collection {
	it.refs = append(
		it.refs,
		ref.New(name, val))

	return it
}

func (it *Collection) AddIf(
	isAdd bool,
	name string,
	val interface{},
) *Collection {
	if !isAdd {
		return it
	}

	it.refs = append(
		it.refs,
		ref.New(name, val))

	return it
}

func (it *Collection) AddPayloadWrapper(
	name string,
	payloadWrapper *corepayload.PayloadWrapper,
) *Collection {
	if payloadWrapper == nil {
		return it.Add(
			name,
			constants.NilAngelBracket)
	}

	var dynamicPayloads string
	if payloadWrapper.HasAttributes() && payloadWrapper.Attributes.HasDynamicPayloads() {
		dynamicPayloads = corejson.BytesToPrettyString(payloadWrapper.Attributes.Payloads())
	}

	return it.AddHashMap(
		map[string]string{
			name + ".attr.json": dynamicPayloads,
			name + ".all":       payloadWrapper.PrettyJsonString(),
		})
}

func (it *Collection) AddJsonResult(
	name string,
	jsonResult *corejson.Result,
) *Collection {
	if jsonResult == nil {
		return it.Add(
			name,
			jsonResult.MeaningfulErrorMessage())
	}

	if jsonResult.HasError() {
		it.Add(name+".error", jsonResult.MeaningfulErrorMessage())
	}

	return it.Add(
		name,
		jsonResult.JsonString())
}

func (it *Collection) Adds(
	refs ...ref.Value,
) *Collection {
	if len(refs) == 0 {
		return it
	}

	for i := range refs {
		it.refs = append(it.refs, refs[i])
	}

	return it
}

func (it *Collection) AddsIf(
	isAdd bool,
	refs ...ref.Value,
) *Collection {
	if !isAdd || len(refs) == 0 {
		return it
	}

	for i := range refs {
		it.refs = append(it.refs, refs[i])
	}

	return it
}

func (it *Collection) AddCollection(
	collection *Collection,
) *Collection {
	if collection.IsEmpty() {
		return it
	}

	return it.Adds(collection.refs...)
}

func (it *Collection) AddCollectionIf(
	isAdd bool,
	collection *Collection,
) *Collection {
	if !isAdd || collection.IsEmpty() {
		return it
	}

	return it.Adds(collection.refs...)
}

func (it *Collection) AddBytes(
	name string,
	rawBytes []byte,
) *Collection {
	if len(rawBytes) == 0 {
		return it.Add(name, "")
	}

	return it.Add(name, string(rawBytes))
}

func (it *Collection) AddInfo(
	infoRef *coretaskinfo.Info,
) *Collection {
	if infoRef == nil {
		return it
	}

	return it.AddHashMap(
		infoRef.Map())
}

func (it *Collection) AddInfoWithPayloads(
	infoRef *coretaskinfo.Info,
	payloads []byte,
) *Collection {
	finalMap := infoRef.MapWithPayload(payloads)

	return it.AddHashMap(finalMap)
}

func (it *Collection) AddInfoWithPayloadAny(
	infoRef *coretaskinfo.Info,
	payloadsAny interface{},
) *Collection {
	finalMap := infoRef.MapWithPayloadAsAny(
		payloadsAny)

	return it.AddHashMap(finalMap)
}

func (it *Collection) AddNull(
	nullObject interface{},
) *Collection {
	if isany.NotNull(nullObject) {
		return it
	}

	typeName := coredynamic.TypeName(nullObject)

	if typeName == "" {
		typeName = "interface{}.(nil)"
	}

	return it.Add("Null.Type", typeName)
}

func (it *Collection) AddManyNulls(
	nullObjects ...interface{},
) *Collection {
	if len(nullObjects) == 0 {
		return it
	}

	slice := make([]string, 0, len(nullObjects))
	for _, currentItem := range nullObjects {
		if isany.NotNull(currentItem) {
			continue
		}

		// null
		slice = append(
			slice,
			fmt.Sprintf(constants.SprintTypeFormat, currentItem))
	}

	if len(slice) == 0 {
		return it
	}

	return it.Add(
		"Null.Many.Types",
		strings.Join(slice, constants.CommaSpace))
}

func (it *Collection) AddEnums(
	name string,
	basicEnumer enuminf.BasicEnumer,
) *Collection {
	if basicEnumer == nil {
		return it.Add(name, "")
	}

	return it.Add(
		name,
		basicEnumer.NameValue())
}

func (it *Collection) AddStringsIf(
	isAdd bool,
	name string,
	lines ...string,
) *Collection {
	if !isAdd {
		return it
	}

	return it.AddStrings(
		name,
		lines...)
}

func (it *Collection) AddStrings(
	name string,
	lines ...string,
) *Collection {
	if len(lines) == 0 {
		return it.Add(name, "")
	}

	return it.Add(
		name,
		corecsv.DefaultCsv(lines...))
}

func (it *Collection) AddBytesIf(
	isAdd bool,
	name string,
	rawBytes []byte,
) *Collection {
	if !isAdd {
		return it
	}

	if len(rawBytes) == 0 {
		return it.Add(name, "")
	}

	return it.Add(name, string(rawBytes))
}

func (it *Collection) AddPayloadBytes(
	payloads []byte,
) *Collection {
	return it.AddBytes(
		"Payloads",
		payloads)
}

func (it *Collection) AddPayloadBytesIf(
	isAdd bool,
	payloads []byte,
) *Collection {
	if !isAdd {
		return it
	}

	return it.AddBytes(
		"Payloads",
		payloads)
}

func (it *Collection) AddCollections(
	collections ...*Collection,
) *Collection {
	if len(collections) == 0 {
		return it
	}

	for _, collection := range collections {
		if collection.IsEmpty() {
			continue
		}

		it.Adds(collection.refs...)
	}

	return it
}

func (it *Collection) AddCollectionCloned(
	collections ...*Collection,
) *Collection {
	if len(collections) == 0 {
		return it
	}

	for _, collection := range collections {
		if collection.IsEmpty() {
			continue
		}

		it.AddsByCloningItems(collection.refs...)
	}

	return it
}

func (it *Collection) ConcatNew(
	isSingleItemsClone bool,
	collections ...*Collection,
) *Collection {
	return NewUsingCollection(
		isSingleItemsClone,
		it,
		collections...)
}

func (it *Collection) concatNewCloneItems(
	length int,
	collections ...*Collection,
) *Collection {
	if len(collections) == 0 {
		return it.ClonePtr()
	}

	clonedNew := New(length)

	if it != nil {
		clonedNew.AddsByCloningItems(it.refs...)
	}

	return clonedNew.
		AddCollectionCloned(collections...)
}

func (it *Collection) AddsPtrByCloningItems(
	refs ...*ref.Value,
) *Collection {
	if len(refs) == 0 {
		return it
	}

	for i := range refs {
		it.refs = append(
			it.refs,
			refs[i].Clone())
	}

	return it
}

func (it *Collection) AddsByCloningItems(
	refs ...ref.Value,
) *Collection {
	if len(refs) == 0 {
		return it
	}

	for i := range refs {
		it.refs = append(
			it.refs,
			refs[i].Clone())
	}

	return it
}

func (it *Collection) AddsPtr(
	refs ...*ref.Value,
) *Collection {
	if len(refs) == 0 {
		return it
	}

	for _, refItem := range refs {
		if refItem == nil {
			continue
		}

		it.refs = append(
			it.refs,
			*refItem)
	}

	return it
}

func (it *Collection) AddMap(
	collectionMap map[string]interface{},
) *Collection {
	if len(collectionMap) == 0 {
		return it
	}

	it.SetRequiresSorting()

	for k, v := range collectionMap {
		it.refs = append(
			it.refs,
			ref.New(k, v))
	}

	return it
}

func (it *Collection) AddHashMap(
	collectionMap map[string]string,
) *Collection {
	if len(collectionMap) == 0 {
		return it
	}

	it.SetRequiresSorting()

	for k, v := range collectionMap {
		it.refs = append(
			it.refs,
			ref.New(k, v))
	}

	return it
}

func (it *Collection) DynamicMap() enumimpl.DynamicMap {
	if it.IsEmpty() {
		return enumimpl.DynamicMap{}
	}

	return it.MapStringAny()
}

func (it *Collection) IsEqual(
	another *Collection,
) bool {
	if it == nil && another == nil {
		return true
	}

	if it == nil || another == nil {
		return false
	}

	if it == another {
		return true
	}

	if it.IsEmpty() && another.IsEmpty() {
		return true
	}

	leftLen := it.Length()
	rightLen := another.Length()

	if leftLen != rightLen {
		return false
	}

	if leftLen == rightLen && leftLen == 0 {
		return true
	}

	anotherRefs := another.refs

	if &anotherRefs == &it.refs {
		return true
	}

	for i, currentRef := range it.refs {
		if !anotherRefs[i].IsEqual(currentRef) {
			return false
		}
	}

	return true
}

func (it *Collection) IsNull() bool {
	return it == nil
}

func (it *Collection) IsEmpty() bool {
	return it == nil ||
		len(it.refs) == 0
}

func (it *Collection) Collection() []ref.Value {
	return it.refs
}

func (it *Collection) List() []ref.Value {
	return it.refs
}

func (it *Collection) Items() []ref.Value {
	return it.refs
}

func (it *Collection) Length() int {
	if it == nil || it.refs == nil {
		return 0
	}

	return len(it.refs)
}

func (it *Collection) Strings() []string {
	if it.IsEmpty() {
		return []string{}
	}

	if it.Length() <= 1 {
		return []string{
			it.refs[0].FullString(),
		}
	}

	stringsCollection := make(
		[]string,
		it.Length())

	mappedItems, sortedKeys := it.RefNameToFullStringMapWithSortedNames()

	for index, key := range sortedKeys {
		stringsCollection[index] = mappedItems[key]
	}

	return stringsCollection
}

// RefNameToFullStringMap
//
//  Key = RefName, Value = FullString of Ref
func (it *Collection) RefNameToFullStringMap() map[string]string {
	newMap := make(map[string]string, it.Length()+1)
	index := 0

	for _, refValue := range it.refs {
		name := refValue.KeyName()
		_, has := newMap[name]
		finalName := name

		if has {
			index++
			finalName = name + strconv.Itoa(index)
		}

		newMap[finalName] = refValue.FullString()
	}

	return newMap
}

// RefNameToFullStringMapWithSortedNames
//
//  Key = RefName, Value = FullString of Ref
func (it *Collection) RefNameToFullStringMapWithSortedNames() (mappedItems map[string]string, sortedNames []string) {
	newMap := make(map[string]string, it.Length()+1)
	allKeys := make([]string, it.Length())
	index := 0

	for keyIndex, refValue := range it.refs {
		name := refValue.KeyName()
		_, has := newMap[name]
		finalName := name

		if has {
			index++
			finalName = name + strconv.Itoa(index)
		}

		allKeys[keyIndex] = finalName
		newMap[finalName] = refValue.FullString()
	}

	sort.Strings(allKeys)

	return newMap, allKeys
}

func (it *Collection) AllKeys() []string {
	allKeys := make([]string, it.Length())
	for i, refVal := range it.refs {
		allKeys[i] = refVal.KeyName()
	}

	return allKeys
}

func (it *Collection) AllKeysSorted() []string {
	allKeys := it.AllKeys()

	sort.Strings(allKeys)

	return allKeys
}

func (it *Collection) String() string {
	if it.IsEmpty() {
		return ""
	}

	return strings.Join(
		it.Strings(),
		constants.CommaSpace)
}

func (it *Collection) Dispose() {
	if it == nil {
		return
	}

	it.refs = nil
}

func (it *Collection) ClonePtr() *Collection {
	if it == nil {
		return nil
	}

	if it.refs != nil {
		refs := make([]ref.Value, it.Length())

		for i, value := range it.refs {
			refs[i] = value.Clone()
		}

		return &Collection{
			refs: refs,
		}
	}

	return EmptyPtr()
}

func (it Collection) Clone() Collection {
	cloned := it.ClonePtr()

	if cloned != nil {
		return *cloned
	}

	return Empty()
}

func (it *Collection) JsonModel() []ref.Value {
	if it == nil {
		return nil
	}

	return it.refs
}

func (it *Collection) JsonModelAny() interface{} {
	return it.JsonModel()
}

func (it *Collection) MarshalJSON() ([]byte, error) {
	return corejson.Serialize.Raw(it.JsonModelAny())
}

func (it *Collection) UnmarshalJSON(
	rawJsonBytes []byte,
) error {
	var referencesDataModel []ref.Value

	err := corejson.Deserialize.UsingBytes(
		rawJsonBytes,
		&referencesDataModel)

	if err == nil {
		it.refs = referencesDataModel
	}

	return err
}

func (it *Collection) ConcatNewUsingItems(
	items ...ref.Value,
) *Collection {
	collection := New(it.Length() + len(items) + 1)

	if it.HasAnyItem() {
		collection.Adds(it.Items()...)
	}

	return collection.Adds(items...)
}

func (it *Collection) Json() corejson.Result {
	return corejson.New(it)
}

func (it *Collection) JsonPtr() *corejson.Result {
	return corejson.NewPtr(it)
}

//goland:noinspection GoLinterLocal
func (it *Collection) ParseInjectUsingJson(
	jsonResult *corejson.Result,
) (*Collection, error) {
	if jsonResult == nil || jsonResult.IsEmptyJsonBytes() {
		return it, defaulterr.UnmarshallingFailedDueToNilOrEmpty
	}

	err := json.Unmarshal(
		jsonResult.Bytes,
		&it)

	if err != nil {
		return it, err
	}

	return it, nil
}

// ParseInjectUsingJsonMust Panic if error
func (it *Collection) ParseInjectUsingJsonMust(
	jsonResult *corejson.Result,
) *Collection {
	newUsingJson, err :=
		it.ParseInjectUsingJson(jsonResult)

	if err != nil {
		panic(err)
	}

	return newUsingJson
}

func (it *Collection) JsonParseSelfInject(
	jsonResult *corejson.Result,
) error {
	_, err := it.ParseInjectUsingJson(
		jsonResult,
	)

	return err
}

func (it *Collection) AsJsonContractsBinder() corejson.JsonContractsBinder {
	return it
}

func (it *Collection) AsJsoner() corejson.Jsoner {
	return it
}

func (it *Collection) AsJsonParseSelfInjector() corejson.JsonParseSelfInjector {
	return it
}

func (it *Collection) AsJsonMarshaller() corejson.JsonMarshaller {
	return it
}

func (it *Collection) HasAnyItem() bool {
	return it.Length() > 0
}

func (it Collection) ToPtr() *Collection {
	return &it
}

func (it *Collection) ToNonPtr() Collection {
	if it == nil {
		return Collection{}
	}

	return *it
}

func (it *Collection) SetRequiresSorting() {
	if it == nil {
		return
	}

	// TODO fix further
	it.isRequiresSorting = true
}
