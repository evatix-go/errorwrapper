package errwrappers

import (
	"gitlab.com/evatix-go/core/codestack"
	"gitlab.com/evatix-go/core/isany"
	"gitlab.com/evatix-go/errorwrapper"
	"gitlab.com/evatix-go/errorwrapper/errnew"
	"gitlab.com/evatix-go/errorwrapper/errtype"
	"gitlab.com/evatix-go/errorwrapper/ref"
)

type NullTracker struct {
	newItems       []*errorwrapper.Wrapper
	prevCollection *Collection
}

func (it *NullTracker) IsNull(anyItem interface{}) (isCollected bool) {
	errWrap := errnew.Null.Simple(anyItem)

	if errWrap.IsEmpty() {
		return false
	}

	// has error
	it.newItems = append(
		it.newItems,
		errWrap)

	it.Commit()

	return true
}

func (it *NullTracker) AddNull(anyItem interface{}) *NullTracker {
	errWrap := errnew.Null.Simple(anyItem)

	if errWrap.IsEmpty() {
		return it
	}

	// has error
	it.newItems = append(
		it.newItems,
		errWrap)

	return it
}

func (it *NullTracker) Error(err error) *NullTracker {
	if err == nil {
		return nil
	}

	errWrap := errorwrapper.NewMsgDisplayErrorNoReference(
		defaultSkipInternal,
		errtype.Null,
		err.Error(),
	)

	return it.ErrorWrapper(errWrap)
}

func (it *NullTracker) ErrorWithMessage(
	message string,
	err error,
) *NullTracker {
	if err == nil {
		return nil
	}

	finalMessage := message + err.Error()

	errWrap := errorwrapper.NewMsgDisplayErrorNoReference(
		defaultSkipInternal,
		errtype.Null,
		finalMessage,
	)

	return it.ErrorWrapper(errWrap)
}

func (it *NullTracker) WithMessage(
	message string,
	objectNull interface{},
) *NullTracker {
	if isany.NotNull(objectNull) {
		return nil
	}

	errWrap := errnew.Null.UsingStackSkip(
		codestack.Skip1,
		message,
		objectNull)

	return it.ErrorWrapper(errWrap)
}

func (it *NullTracker) WithMessageReferences(
	message string,
	objectNull interface{},
	references ...ref.Value,
) *NullTracker {
	if isany.NotNull(objectNull) {
		return nil
	}

	errWrap := errnew.Null.UsingStackSkip(
		codestack.Skip1,
		message,
		objectNull,
		references...)

	return it.ErrorWrapper(errWrap)
}

func (it *NullTracker) ErrorWrapper(
	errorWrapper *errorwrapper.Wrapper,
) *NullTracker {
	if errorWrapper.IsEmpty() {
		return nil
	}

	it.newItems = append(
		it.newItems,
		errorWrapper)

	return it
}

func (it *NullTracker) AddNulls(
	anyItems ...interface{},
) *NullTracker {
	errWrap := errnew.Null.ManyByCheckingUsingStackSkip(
		codestack.Skip1,
		anyItems...)

	if errWrap.IsEmpty() {
		return it
	}

	// has error
	it.newItems = append(
		it.newItems,
		errWrap)

	return it
}

func (it *NullTracker) IsNulls(
	anyItems ...interface{},
) (isAnyNullCollected bool) {
	errWrap := errnew.Null.ManyByCheckingUsingStackSkip(
		codestack.Skip1,
		anyItems...)

	if errWrap.IsEmpty() {
		return false
	}

	// has error
	it.newItems = append(
		it.newItems,
		errWrap)

	it.Commit()

	return true
}

func (it *NullTracker) AddMsgNull(
	msg string,
	anyItem interface{},
) *NullTracker {
	errWrap := errnew.Null.WithMessage(
		msg,
		anyItem)

	if errWrap.IsEmpty() {
		return it
	}

	// has error
	it.newItems = append(
		it.newItems,
		errWrap)

	return it
}

func (it *NullTracker) AddMsgNulls(
	msg string,
	anyItems ...interface{},
) *NullTracker {
	errWrap := errnew.Null.ManyWithMessage(
		msg,
		anyItems...)

	if errWrap.IsEmpty() {
		return it
	}

	// has error
	it.newItems = append(
		it.newItems,
		errWrap)

	return it
}

func (it *NullTracker) AddMsgNullRefs(
	msg string,
	anyItem interface{},
	references ...ref.Value,
) *NullTracker {
	errWrap := errnew.Null.WithRefs(
		msg,
		anyItem,
		references...)

	if errWrap.IsEmpty() {
		return it
	}

	// has error
	it.newItems = append(
		it.newItems,
		errWrap)

	return it
}

func (it *NullTracker) Clear() {
	it.newItems = nil
}

func (it *NullTracker) Items() []*errorwrapper.Wrapper {
	return it.newItems
}

func (it *NullTracker) Commit() *Collection {
	collection := it.prevCollection.AddWrappers(
		it.newItems...)

	it.Clear()

	return collection
}

func (it *NullTracker) CommitUsingLock() *Collection {
	globalMutex.Lock()
	defer globalMutex.Unlock()

	return it.Commit()
}

func (it *NullTracker) Length() int {
	if it == nil {
		return 0
	}

	return len(it.newItems)
}

func (it *NullTracker) IsEmpty() bool {
	return it.Length() == 0
}

func (it *NullTracker) HasAnyItem() bool {
	return it.Length() > 0
}

func (it *NullTracker) HasAnyChanges() bool {
	return it.Length() > 0
}

func (it *NullTracker) HasAnyIssues() bool {
	return it.Length() > 0
}

func (it *NullTracker) NewCollection() *Collection {
	if it == nil || len(it.newItems) == 0 {
		return Empty()
	}

	return New(it.Length()).AddWrappers(it.newItems...)
}

func (it *NullTracker) IsAnyCollected() (isCollected bool) {
	isCollected = len(it.newItems) > 0
	it.Commit()

	return isCollected
}

func (it *NullTracker) IsNoneCollected() (isNothingCollected bool) {
	return !it.IsAnyCollected()
}

func (it *NullTracker) IsSuccess() (isSuccess bool) {
	return !it.IsAnyCollected()
}

func (it *NullTracker) IsFailed() (isFailed bool) {
	return it.IsAnyCollected()
}
