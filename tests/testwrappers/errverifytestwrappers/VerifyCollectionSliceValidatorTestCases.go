package errverifytestwrappers

import (
	"errors"

	"gitlab.com/evatix-go/core/constants"
	"gitlab.com/evatix-go/core/coredata/corestr"
	"gitlab.com/evatix-go/core/enums/stringcompareas"
	"gitlab.com/evatix-go/errorwrapper"
	"gitlab.com/evatix-go/errorwrapper/errnew"
	"gitlab.com/evatix-go/errorwrapper/errtype"
	"gitlab.com/evatix-go/errorwrapper/errverify"
)

var VerifyCollectionSliceValidatorTestCases = []VerifyErrorCollectionTestWrapper{
	{
		InputErrorCollections: []*errorwrapper.Wrapper{
			errnew.Type.Default(errtype.InvalidOption, errors.New("some thing went wrong")),
			errnew.Type.Default(errtype.NotSupportInWindows, errors.New("some thing went wrong: NotSupportInWindows")),
			errnew.Type.Default(errtype.NotSupportedOption, errors.New("some thing went wrong: NotSupportedOption")),
		},
		Verifier: errverify.CollectionVerifier{
			Verifier: errverify.Verifier{
				Header:       "Collection has 3 elements with 3 wrappers, verify length and it's content",
				FunctionName: VerifyCollectionIsMatch,
				VerifyAs:     stringcompareas.Equal,
				IsPrintError: true,
			},
			ExpectationLines: &corestr.SimpleSlice{
				Items: []string{
					"[Error (InvalidOption - #469): Selected option is invalid! some thing went wrong.]",
					"[Error (NotSupportInWindows - #93): Current request is not supported in Windows Operating system. some thing went wrong: NotSupportInWindows.]",
					"[Error (NotSupportedOption - #107): None of the option is supported. some thing went wrong: NotSupportedOption.]",
				},
			},
			ErrorLength: 3,
		},
	},
	{
		InputErrorCollections: []*errorwrapper.Wrapper{
			errnew.Type.Default(errtype.InvalidOption, errors.New("some thing went wrong")),
			errnew.Type.Default(errtype.NotSupportInWindows, errors.New("some thing went wrong: NotSupportInWindows")),
			errnew.Type.Default(errtype.NotSupportedOption, errors.New("some thing went wrong: NotSupportedOption")),
		},
		Verifier: errverify.CollectionVerifier{
			Verifier: errverify.Verifier{
				Header:       "Collection has 3 elements with 3 wrappers, verify it's content",
				FunctionName: VerifyCollectionIsMatch,
				VerifyAs:     stringcompareas.Equal,
				IsPrintError: true,
			},
			ExpectationLines: &corestr.SimpleSlice{
				Items: []string{
					"[Error (InvalidOption - #469): Selected option is invalid! some thing went wrong.]",
					"[Error (NotSupportInWindows - #93): Current request is not supported in Windows Operating system. some thing went wrong: NotSupportInWindows.]",
					"[Error (NotSupportedOption - #107): None of the option is supported. some thing went wrong: NotSupportedOption.]",
				},
			},
			ErrorLength: constants.InvalidValue,
		},
	},
}
