package linuxservicecmd

import (
	"gitlab.com/evatix-go/enum/servicestate"
	"gitlab.com/evatix-go/errorwrapper"
)

func StartQuick(
	servicesName string,
) *errorwrapper.Wrapper {
	_, errWrap := RunAction(
		true,
		false,
		servicestate.Start,
		servicesName)

	return errWrap
}
