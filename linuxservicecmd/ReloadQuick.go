package linuxservicecmd

import (
	"gitlab.com/evatix-go/enum/servicestate"
	"gitlab.com/evatix-go/errorwrapper"
)

func ReloadQuick(
	servicesName string,
) *errorwrapper.Wrapper {
	_, errWrap := RunAction(
		true,
		false,
		servicestate.Reload,
		servicesName)

	return errWrap
}
