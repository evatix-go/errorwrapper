package errorwrapper

import "gitlab.com/evatix-go/core/constants"

const (
	defaultSkipInternal = constants.One
	MessagesJoiner      = constants.Space
)
