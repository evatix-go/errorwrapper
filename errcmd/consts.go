package errcmd

import (
	"gitlab.com/evatix-go/errorwrapper/internal/consts"
)

const (
	InvalidExitCode             = consts.InvalidExitCode
	SuccessfullyRunningExitCode = consts.CmdSuccessfullyRunningExitCode
	SingleLineScriptsJoiner     = " && " // " && "
	changeDirSpace              = "cd "
	ScriptsMultiLineJoiner      = " && \\ \n" // " && \\ \n"
)
