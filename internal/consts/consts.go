package consts

import "gitlab.com/evatix-go/core/constants"

const (
	InvalidExitCode                = constants.MinInt
	CmdSuccessfullyRunningExitCode = constants.Zero
	DefaultErrorLineSeparator      = constants.DefaultLine
	MaxUnit8AsFloat64              = float64(constants.MaxUnit8)
)
