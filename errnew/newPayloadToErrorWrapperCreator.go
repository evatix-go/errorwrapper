package errnew

import (
	"gitlab.com/evatix-go/core/coredata/corejson"
	"gitlab.com/evatix-go/core/coredata/corepayload"
	"gitlab.com/evatix-go/core/coretaskinfo"
	"gitlab.com/evatix-go/errorwrapper"
	"gitlab.com/evatix-go/errorwrapper/errtype"
	"gitlab.com/evatix-go/errorwrapper/refs"
)

type newPayloadToErrorWrapperCreator struct{}

func (it newPayloadToErrorWrapperCreator) Create(
	payloadWrapper *corepayload.PayloadWrapper,
) *errorwrapper.Wrapper {
	if payloadWrapper.IsEmptyError() {
		return nil
	}

	return errorwrapper.NewUsingBasicErr(
		payloadWrapper.
			Attributes.
			BasicErrWrapper)
}

func (it newPayloadToErrorWrapperCreator) InfoPayloadAny(
	variation errtype.Variation,
	info *coretaskinfo.Info,
	payloadsAny interface{},
) *errorwrapper.Wrapper {
	refsCollection := refs.NewUsingInfoPayloadAny(
		info,
		payloadsAny)

	refsCollection.AddReferences()

	return errorwrapper.NewMsgDisplayError(
		defaultSkipInternal,
		variation,
		"",
		refsCollection)
}

func (it newPayloadToErrorWrapperCreator) CreateUsingInfo(
	info *coretaskinfo.Info,
	payloadWrapper *corepayload.PayloadWrapper,
) *errorwrapper.Wrapper {
	if payloadWrapper.IsEmptyError() {
		return nil
	}

	refsCollection := refs.NewUsingInfoPayloadAny(
		info,
		payloadWrapper)

	refsCollection.AddReferences()

	errWrap := errorwrapper.NewUsingBasicErr(
		payloadWrapper.
			Attributes.
			BasicErrWrapper)

	return errWrap.ConcatNew().MsgRefsOnly(
		refsCollection.Items()...)
}

func (it newPayloadToErrorWrapperCreator) DeserializeAnyTo(
	anyItem interface{},
) (
	payloadWrapper *corepayload.PayloadWrapper,
	payloadErr,
	parsingErr *errorwrapper.Wrapper,
) {
	if payloadWrapper.IsNull() {
		return nil, nil, Null.Simple(anyItem)
	}

	payloadWrapper, parseRawErr := corepayload.New.PayloadWrapper.CastOrDeserializeFrom(
		anyItem)

	if parseRawErr != nil {
		return payloadWrapper, it.Create(payloadWrapper), Type.Default(
			errtype.ParsingFailed,
			parseRawErr)
	}

	return payloadWrapper, it.Create(payloadWrapper), nil
}

func (it newPayloadToErrorWrapperCreator) DeserializeAnyToCombinedErr(
	anyItem interface{},
) (
	payloadWrapper *corepayload.PayloadWrapper,
	combinedPayloadErrOrParsingErr *errorwrapper.Wrapper,
) {
	payloadWrapper, payloadErr, parsingErr := it.DeserializeAnyTo(anyItem)

	if parsingErr.IsEmpty() && payloadErr.IsEmpty() {
		return payloadWrapper, nil
	}

	return payloadWrapper, payloadErr.ConcatNew().Wrapper(parsingErr)
}

func (it newPayloadToErrorWrapperCreator) DeserializeBytesToCombinedErr(
	jsonByes []byte,
) (
	payloadWrapper *corepayload.PayloadWrapper,
	combinedPayloadErrOrParsingErr *errorwrapper.Wrapper,
) {
	payloadWrapper, parseRawErr := corepayload.
		New.
		PayloadWrapper.
		Deserialize(
			jsonByes)

	if parseRawErr != nil {
		return payloadWrapper, Unmarshal.Error(parseRawErr)
	}

	return payloadWrapper, it.Create(payloadWrapper)
}

func (it newPayloadToErrorWrapperCreator) Error(
	variation errtype.Variation,
	err error,
	payloads []byte,
) *errorwrapper.Wrapper {
	if err == nil {
		return nil
	}

	refCollection := refs.New2()
	refCollection.AddPayloadBytes(payloads)

	return errorwrapper.NewMsgDisplayError(
		defaultSkipInternal,
		variation,
		err.Error(),
		refCollection)
}

func (it newPayloadToErrorWrapperCreator) ErrorRefAsAnyToBytes(
	variation errtype.Variation,
	err error,
	reference interface{},
) *errorwrapper.Wrapper {
	if err == nil {
		return nil
	}

	refCollection := refs.New2()
	refCollection.AddJsonResult(
		"Reference",
		corejson.AnyTo.SerializedJsonResult(reference))

	return errorwrapper.NewMsgDisplayError(
		defaultSkipInternal,
		variation,
		err.Error(),
		refCollection)
}
