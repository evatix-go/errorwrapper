package errnew

import (
	"gitlab.com/evatix-go/core/coredata/corejson"
	"gitlab.com/evatix-go/core/coreinterface"
	"gitlab.com/evatix-go/core/coreinterface/errcoreinf"
	"gitlab.com/evatix-go/core/errcore"
	"gitlab.com/evatix-go/core/isany"
	"gitlab.com/evatix-go/errorwrapper"
	"gitlab.com/evatix-go/errorwrapper/errtype"
)

type newErrorInterfaceToWrapperCreator struct{}

func (it newErrorInterfaceToWrapperCreator) Default(
	variation errtype.Variation,
	errInf errcoreinf.BaseErrorOrCollectionWrapper,
) *errorwrapper.Wrapper {
	if errInf == nil || errInf.IsEmpty() {
		return nil
	}

	return errorwrapper.InterfaceToErrorWrapper(
		variation,
		errInf)
}

// CastAnyToCompiledErrWrap
//
//  tries may ways to get to the error wrapper.
//  on fail returns parsed error
//
// Steps:
//  - *errorwrapper.Wrapper
//  - errorwrapper.Wrapper
//  - errorwrapper.ErrWrapper
//  - *errcore.RawErrCollection
//  - errcore.RawErrCollection
//  - errcoreinf.BaseRawErrCollectionDefiner
//  - errcoreinf.BasicErrWrapper
//  - errcoreinf.BaseErrorWrapperCollectionDefiner
//  - errcoreinf.BaseErrorOrCollectionWrapper
//  - corejson.Result
//  - *corejson.Result
//  - []byte
//  - error
//  - string
//  - Jsoner
//  - Serializer
//  - FullStringer
//  - MeaningFullErrorGetter
//  - ErrWrapGetter
func (it newErrorInterfaceToWrapperCreator) CastAnyToCompiledErrWrap(
	variation errtype.Variation,
	errInf interface{},
) (errWrapCombinedWithParsingErrWrap *errorwrapper.Wrapper) {
	errWrap, parsingErrWrap := it.CastAnyTo(
		variation,
		errInf)

	return errWrap.ConcatNew().WrapperUsingStackSkip(
		defaultSkipInternal,
		parsingErrWrap,
	)
}

// CastAnyTo
//
//  tries may ways to get to the error wrapper.
//  on fail returns parsed error
//
// Steps:
//  - *errorwrapper.Wrapper
//  - errorwrapper.Wrapper
//  - errorwrapper.ErrWrapper
//  - *errcore.RawErrCollection
//  - errcore.RawErrCollection
//  - errcoreinf.BaseRawErrCollectionDefiner
//  - errcoreinf.BasicErrWrapper
//  - errcoreinf.BaseErrorWrapperCollectionDefiner
//  - errcoreinf.BaseErrorOrCollectionWrapper
//  - corejson.Result
//  - *corejson.Result
//  - []byte
//  - error
//  - string
//  - Jsoner
//  - Serializer
//  - FullStringer
//  - MeaningFullErrorGetter
//  - ErrWrapGetter
func (it newErrorInterfaceToWrapperCreator) CastAnyTo(
	variation errtype.Variation,
	errInf interface{},
) (errWrap *errorwrapper.Wrapper, parsingErrWrap *errorwrapper.Wrapper) {
	if errInf == nil || isany.Null(errInf) {
		return nil, nil
	}

	switch casted := errInf.(type) {
	case *errorwrapper.Wrapper:
		return casted, nil
	case errorwrapper.Wrapper:
		return casted.Ptr(), nil
	case errorwrapper.ErrWrapper:
		return casted.AsErrorWrapper(), nil

	case *errcore.RawErrCollection:
		return it.ActualRawErrCollection(
			variation,
			casted), nil
	case errcore.RawErrCollection:
		return it.ActualRawErrCollection(
			variation,
			casted.ToRawErrCollection()), nil

	case errcoreinf.BaseRawErrCollectionDefiner:
		return it.RawErrCollection(
			variation,
			casted), nil

	case errcoreinf.BasicErrWrapper:
		return it.BasicErr(casted), nil
	case errcoreinf.BaseErrorWrapperCollectionDefiner:
		return it.BasicErr(
			casted.GetAsBasicWrapperUsingTyper(
				variation.AsBasicErrorTyper())), nil
	case errcoreinf.BaseErrorOrCollectionWrapper:
		return it.Default(variation, casted), nil
	case corejson.Result:
		return DeserializeTo.JsonResultToWrapperUsingStackSkip(
			defaultSkipInternal,
			&casted)
	case *corejson.Result:
		return DeserializeTo.JsonResultToWrapperUsingStackSkip(
			defaultSkipInternal,
			casted)
	case []byte:
		return DeserializeTo.BytesToWrapperUsingStackSkip(
			defaultSkipInternal,
			casted)
	case error:
		convErrWp, parsedFailed := DeserializeTo.JsonErrToWrapper(
			casted)

		if parsedFailed.HasAnyIssues() {
			// create general error
			return Type.Error(variation, casted), nil
		}

		return convErrWp, nil
	case string:
		return DeserializeTo.JsonStringToWrapper(
			false,
			casted)
	case corejson.Jsoner:
		return it.CastAnyTo(
			variation,
			casted.JsonPtr())
	case coreinterface.Serializer:
		return it.CastAnyTo(
			variation,
			corejson.NewResult.UsingSerializer(casted))
	case coreinterface.FullStringer:
		return Message.Default(
			variation,
			casted.FullString()), nil

	case coreinterface.MeaningFullErrorGetter:
		return Message.Default(
			variation,
			errcore.ToString(casted.MeaningFullError())), nil
	case errorwrapper.ErrWrapGetter:
		return casted.ErrWrap(), nil
	}

	return nil, FromTo.Message(errtype.CastingFailed,
		"cannot cast or make to error collection",
		errInf, &errorwrapper.Wrapper{})
}

// AnyType
//
//  tries may ways to get to the error wrapper.
//  on fail returns parsed error
//
// Steps:
//  - *errorwrapper.Wrapper
//  - errorwrapper.Wrapper
//  - errorwrapper.ErrWrapper
//  - *errcore.RawErrCollection
//  - errcore.RawErrCollection
//  - errcoreinf.BaseRawErrCollectionDefiner
//  - errcoreinf.BasicErrWrapper
//  - errcoreinf.BaseErrorWrapperCollectionDefiner
//  - errcoreinf.BaseErrorOrCollectionWrapper
//  - corejson.Result
//  - *corejson.Result
//  - []byte
//  - error
//  - string
//  - Jsoner
//  - Serializer
//  - FullStringer
//  - MeaningFullErrorGetter
//  - ErrWrapGetter
func (it newErrorInterfaceToWrapperCreator) AnyType(
	variation errtype.Variation,
	errInf interface{},
) (convertedErrWrapper *errorwrapper.Wrapper, parsedErrWp *errorwrapper.Wrapper) {
	if isany.Null(errInf) {
		return nil, nil
	}

	return it.CastAnyTo(variation, errInf)
}

func (it newErrorInterfaceToWrapperCreator) ActualRawErrCollection(
	variation errtype.Variation,
	rawErrCollection *errcore.RawErrCollection,
) *errorwrapper.Wrapper {
	if rawErrCollection == nil || rawErrCollection.IsEmpty() {
		return nil
	}

	return errorwrapper.NewMsgDisplayErrorNoReference(
		defaultSkipInternal,
		variation,
		rawErrCollection.CompiledError().Error())
}

// NoType
//
//  tries to cast to error wrapper first if not possible then creates new one.
//  using no type
func (it newErrorInterfaceToWrapperCreator) NoType(
	errInf errcoreinf.BaseErrorOrCollectionWrapper,
) *errorwrapper.Wrapper {
	if errInf == nil || errInf.IsEmpty() {
		return nil
	}

	return errorwrapper.InterfaceToErrorWrapper(
		errtype.Unknown,
		errInf)
}

func (it newErrorInterfaceToWrapperCreator) BasicErr(
	basicErr errcoreinf.BasicErrWrapper,
) *errorwrapper.Wrapper {
	if basicErr == nil || basicErr.IsEmpty() {
		return nil
	}

	return errorwrapper.NewUsingBasicErr(basicErr)
}

func (it newErrorInterfaceToWrapperCreator) RawErrCollection(
	variation errtype.Variation,
	rawErrCollection errcoreinf.BaseRawErrCollectionDefiner,
) *errorwrapper.Wrapper {
	if rawErrCollection == nil || rawErrCollection.IsEmpty() {
		return nil
	}

	return errorwrapper.NewMsgDisplayErrorNoReference(
		defaultSkipInternal,
		variation,
		rawErrCollection.CompiledStackTracesString())
}

func (it newErrorInterfaceToWrapperCreator) ErrorWrapperCollectionDefiner(
	variation errtype.Variation,
	collection errcoreinf.BaseErrorWrapperCollectionDefiner,
) *errorwrapper.Wrapper {
	if collection == nil || collection.IsEmpty() {
		return nil
	}

	basicErr := collection.GetAsBasicWrapperUsingTyper(
		variation.AsBasicErrorTyper())

	return errorwrapper.NewUsingBasicErr(basicErr)
}

func (it newErrorInterfaceToWrapperCreator) ErrorWrapperCollectionsDefiner(
	variation errtype.Variation,
	collections ...errcoreinf.BaseErrorWrapperCollectionDefiner,
) *errorwrapper.Wrapper {
	if collections == nil || len(collections) == 0 {
		return nil
	}

	rawErrCollection := errcore.RawErrCollection{}

	for _, collection := range collections {
		basicErr := collection.GetAsBasicWrapper()

		if basicErr == nil || basicErr.IsEmpty() {
			continue
		}

		rawErrCollection.AddError(
			basicErr.CompiledErrorWithStackTraces())
	}

	if rawErrCollection.IsEmpty() {
		return nil
	}

	return errorwrapper.NewMsgDisplayErrorNoReference(
		defaultSkipInternal,
		variation,
		rawErrCollection.CompiledStackTracesString())
}
