package errnew

import (
	"gitlab.com/evatix-go/core/codestack"
	"gitlab.com/evatix-go/core/constants"
	"gitlab.com/evatix-go/core/coredata/corejson"
	"gitlab.com/evatix-go/core/coretaskinfo"
	"gitlab.com/evatix-go/enum/logtype"
	"gitlab.com/evatix-go/errorwrapper"
	"gitlab.com/evatix-go/errorwrapper/refs"
)

type newFriendlyErrorCreator struct{}

func (it newFriendlyErrorCreator) Create(
	friendlyMessage string,
	errWrap *errorwrapper.Wrapper,
) *errorwrapper.FriendlyError {
	if errWrap.IsEmpty() {
		return nil
	}

	return &errorwrapper.FriendlyError{
		FriendlyErrorMessage: friendlyMessage,
		ErrorWrapper:         errWrap,
	}
}

func (it newFriendlyErrorCreator) SingleExample(
	friendlyMessage string,
	errWrap *errorwrapper.Wrapper,
	singleExample string,
) *errorwrapper.FriendlyError {
	if errWrap.IsEmpty() {
		return nil
	}

	info := coretaskinfo.New.Info.Plain.SingleExampleOnly(
		singleExample)
	wrappedErr := it.WrapErrorWithDetailsNoPayloads(
		errWrap,
		info)

	return &errorwrapper.FriendlyError{
		FriendlyErrorMessage: friendlyMessage,
		Info:                 info,
		ErrorWrapper:         wrappedErr,
	}
}

func (it newFriendlyErrorCreator) ExistingWrapWithUrl(
	friendlyMessage string,
	errWrap *errorwrapper.Wrapper,
	url string,
) *errorwrapper.FriendlyError {
	if errWrap.IsEmpty() {
		return nil
	}

	info := coretaskinfo.New.Info.Plain.UrlOnly(
		url)

	wrappedErr := it.WrapErrorWithDetailsNoPayloads(
		errWrap,
		info)

	return &errorwrapper.FriendlyError{
		FriendlyErrorMessage: friendlyMessage,
		Info:                 info,
		ErrorWrapper:         wrappedErr,
	}
}

func (it newFriendlyErrorCreator) ExistingWrapWithUrlExamples(
	friendlyMessage string,
	errWrap *errorwrapper.Wrapper,
	url string,
	examples ...string,
) *errorwrapper.FriendlyError {
	if errWrap.IsEmpty() {
		return nil
	}

	info := coretaskinfo.New.Info.Plain.NameDescUrlExamples(
		"",  // name
		"",  // desc
		url, // url
		examples...)

	wrappedErr := it.WrapErrorWithDetailsNoPayloads(
		errWrap,
		info)

	return &errorwrapper.FriendlyError{
		FriendlyErrorMessage: friendlyMessage,
		Info:                 info,
		ErrorWrapper:         wrappedErr,
	}
}

func (it newFriendlyErrorCreator) ExamplesOnly(
	friendlyMessage string,
	errWrap *errorwrapper.Wrapper,
	examples ...string,
) *errorwrapper.FriendlyError {
	if errWrap.IsEmpty() {
		return nil
	}

	info := coretaskinfo.New.Info.Plain.ExamplesOnly(
		examples...)
	wrappedErr := it.WrapErrorWithDetailsNoPayloads(
		errWrap,
		info)

	return &errorwrapper.FriendlyError{
		FriendlyErrorMessage: friendlyMessage,
		Info:                 info,
		ErrorWrapper:         wrappedErr,
	}
}

func (it newFriendlyErrorCreator) AllUrls(
	friendlyMessage string,
	errWrap *errorwrapper.Wrapper,
	name, desc,
	url, hintUrl, errUrl, exampleUrl string,
) *errorwrapper.FriendlyError {
	if errWrap.IsEmpty() {
		return nil
	}

	info := coretaskinfo.Info{
		RootName:    name,
		Description: desc,
		Url:         url,
		HintUrl:     hintUrl,
		ErrorUrl:    errUrl,
		ExampleUrl:  exampleUrl,
	}.ToPtr()

	wrappedErr := it.WrapErrorWithDetailsNoPayloads(
		errWrap,
		info)

	return &errorwrapper.FriendlyError{
		FriendlyErrorMessage: friendlyMessage,
		Info:                 info,
		ErrorWrapper:         wrappedErr,
	}
}

func (it newFriendlyErrorCreator) All(
	friendlyMessage string,
	errWrap *errorwrapper.Wrapper,
	logType logtype.Variant,
	info *coretaskinfo.Info,
	payloads []byte,
) *errorwrapper.FriendlyError {
	if errWrap.IsEmpty() {
		return nil
	}

	wrappedErr := it.WrapErrorWithDetailsPayloads(
		errWrap,
		info,
		payloads)

	return &errorwrapper.FriendlyError{
		FriendlyErrorMessage: friendlyMessage,
		LogType:              logType,
		Info:                 info,
		Payloads:             payloads,
		ErrorWrapper:         wrappedErr,
	}
}

func (it newFriendlyErrorCreator) AllSecure(
	friendlyMessage string,
	errWrap *errorwrapper.Wrapper,
	logType logtype.Variant,
	info *coretaskinfo.Info,
	payloads []byte,
) *errorwrapper.FriendlyError {
	if errWrap.IsEmpty() {
		return nil
	}

	info = it.SecureInfo(info)
	wrappedErr := it.WrapErrorWithDetailsNoPayloads(
		errWrap,
		info,
	)

	return &errorwrapper.FriendlyError{
		FriendlyErrorMessage: friendlyMessage,
		LogType:              logType,
		Info:                 info,
		Payloads:             payloads,
		ErrorWrapper:         wrappedErr,
	}
}

func (it newFriendlyErrorCreator) SecureInfoIf(
	isSecure bool,
	info *coretaskinfo.Info,
) *coretaskinfo.Info {
	if !isSecure {
		return info
	}

	return it.SecureInfo(info)
}

func (it newFriendlyErrorCreator) SecureInfo(
	info *coretaskinfo.Info,
) *coretaskinfo.Info {
	if info == nil {
		return &coretaskinfo.Info{
			ExcludeOptions: &coretaskinfo.ExcludingOptions{
				IsSecureText: true,
			},
		}
	} else if info.IsPlainText() {
		return info.SetSecure()
	}

	// already secure
	return info
}

func (it newFriendlyErrorCreator) newSecureInfo() *coretaskinfo.Info {
	return &coretaskinfo.Info{
		ExcludeOptions: &coretaskinfo.ExcludingOptions{
			IsSecureText: true,
		},
	}
}

func (it newFriendlyErrorCreator) PayloadSecure(
	friendlyMessage string,
	errWrap *errorwrapper.Wrapper,
	payloads []byte,
) *errorwrapper.FriendlyError {
	if errWrap.IsEmpty() {
		return nil
	}

	info := it.newSecureInfo()
	wrappedErr := it.WrapErrorWithDetailsNoPayloads(
		errWrap,
		info)

	return &errorwrapper.FriendlyError{
		FriendlyErrorMessage: friendlyMessage,
		Info:                 info,
		Payloads:             payloads,
		ErrorWrapper:         wrappedErr,
	}
}

func (it newFriendlyErrorCreator) Payloads(
	friendlyMessage string,
	errWrap *errorwrapper.Wrapper,
	payloads []byte,
) *errorwrapper.FriendlyError {
	if errWrap.IsEmpty() {
		return nil
	}

	wrappedErr := it.IncludePayloadsOnly(
		errWrap,
		payloads)

	return &errorwrapper.FriendlyError{
		FriendlyErrorMessage: friendlyMessage,
		Payloads:             payloads,
		ErrorWrapper:         wrappedErr,
	}
}

func (it newFriendlyErrorCreator) PayloadsInfo(
	friendlyMessage string,
	errWrap *errorwrapper.Wrapper,
	info *coretaskinfo.Info,
	payloads []byte,
) *errorwrapper.FriendlyError {
	if errWrap.IsEmpty() {
		return nil
	}

	wrappedErr := it.IncludePayloadsOnly(
		errWrap,
		payloads)

	return &errorwrapper.FriendlyError{
		FriendlyErrorMessage: friendlyMessage,
		Info:                 info,
		Payloads:             payloads,
		ErrorWrapper:         wrappedErr,
	}
}

func (it newFriendlyErrorCreator) PayloadsAnyInfo(
	friendlyMessage string,
	errWrap *errorwrapper.Wrapper,
	info *coretaskinfo.Info,
	payloadsAny interface{},
) *errorwrapper.FriendlyError {
	if errWrap.IsEmpty() {
		return nil
	}

	wrappedErr := it.IncludePayloadsAnyOnly(
		errWrap,
		payloadsAny)

	return &errorwrapper.FriendlyError{
		FriendlyErrorMessage: friendlyMessage,
		Info:                 info,
		Payloads:             corejson.Serialize.ToSafeBytesSwallowErr(payloadsAny),
		ErrorWrapper:         wrappedErr,
	}
}

func (it newFriendlyErrorCreator) PayloadInfoLogTypeSecure(
	friendlyMessage string,
	errWrap *errorwrapper.Wrapper,
	variant logtype.Variant,
	info *coretaskinfo.Info,
	payloads []byte,
) *errorwrapper.FriendlyError {
	if errWrap.IsEmpty() {
		return nil
	}

	return it.PayloadInfoLogType(
		true,
		friendlyMessage,
		errWrap,
		variant,
		info,
		payloads)
}

func (it newFriendlyErrorCreator) PayloadInfoLogTypePlain(
	friendlyMessage string,
	errWrap *errorwrapper.Wrapper,
	variant logtype.Variant,
	info *coretaskinfo.Info,
	payloads []byte,
) *errorwrapper.FriendlyError {
	if errWrap.IsEmpty() {
		return nil
	}

	return it.PayloadInfoLogType(
		false,
		friendlyMessage,
		errWrap,
		variant,
		info,
		payloads)
}

func (it newFriendlyErrorCreator) PayloadInfoLogType(
	isSecure bool,
	friendlyMessage string,
	errWrap *errorwrapper.Wrapper,
	variant logtype.Variant,
	info *coretaskinfo.Info,
	payloads []byte,
) *errorwrapper.FriendlyError {
	if errWrap.IsEmpty() {
		return nil
	}

	info = it.SecureInfoIf(
		isSecure,
		info)

	if !isSecure && info.IsSecure() {
		// set plain, if plain is selected
		info.ExcludeOptions.IsSecureText = false
	}

	finalErr := it.WrapErrorWithDetailsPayloads(
		errWrap,
		info,
		payloads)

	return &errorwrapper.FriendlyError{
		FriendlyErrorMessage: friendlyMessage,
		LogType:              variant,
		Info:                 info,
		Payloads:             payloads,
		ErrorWrapper:         finalErr,
	}
}

func (it newFriendlyErrorCreator) UrlExamples(
	friendlyMessage string,
	errWrap *errorwrapper.Wrapper,
	taskName, desc string,
	url string,
	examples ...string,
) *errorwrapper.FriendlyError {
	if errWrap.IsEmpty() {
		return nil
	}

	info := coretaskinfo.New.Info.Plain.NameDescUrlExamples(
		taskName,
		desc,
		url,
		examples...,
	)

	finalErr := it.WrapErrorWithDetailsNoPayloads(
		errWrap,
		info,
	)

	return &errorwrapper.FriendlyError{
		FriendlyErrorMessage: friendlyMessage,
		Info:                 info,
		ErrorWrapper:         finalErr,
	}
}

func (it newFriendlyErrorCreator) NameDescription(
	friendlyMessage string,
	errWrap *errorwrapper.Wrapper,
	taskName, desc string,
) *errorwrapper.FriendlyError {
	if errWrap.IsEmpty() {
		return nil
	}

	info := coretaskinfo.New.Info.Default(
		taskName,
		desc,
		"", // url
	)

	finalErr := it.WrapErrorWithDetailsNoPayloads(
		errWrap,
		info,
	)

	return &errorwrapper.FriendlyError{
		FriendlyErrorMessage: friendlyMessage,
		Info:                 info,
		ErrorWrapper:         finalErr,
	}
}

func (it newFriendlyErrorCreator) NameDescriptionErrorUrlExamples(
	friendlyMessage string,
	errWrap *errorwrapper.Wrapper,
	taskName, desc, errUrl string,
	examples ...string,
) *errorwrapper.FriendlyError {
	if errWrap.IsEmpty() {
		return nil
	}

	info := coretaskinfo.New.Info.Plain.NameDescUrlErrUrlExamples(
		taskName,
		desc,
		"", // url
		errUrl,
		examples...)

	finalErr := it.WrapErrorWithDetailsNoPayloads(
		errWrap,
		info,
	)

	return &errorwrapper.FriendlyError{
		FriendlyErrorMessage: friendlyMessage,
		Info:                 info,
		ErrorWrapper:         finalErr,
	}
}

func (it newFriendlyErrorCreator) CompiledReferencesNoPayloads(
	errWrap *errorwrapper.Wrapper,
	info *coretaskinfo.Info,
) refs.Collection {
	return it.CompiledReferences(
		false,
		errWrap,
		info,
		nil)
}

func (it newFriendlyErrorCreator) CompiledReferencesPayloads(
	errWrap *errorwrapper.Wrapper,
	info *coretaskinfo.Info,
	payloads []byte,
) refs.Collection {
	return it.CompiledReferences(
		true,
		errWrap,
		info,
		payloads)
}

func (it newFriendlyErrorCreator) CompiledReferences(
	hasPayload bool,
	errWrap *errorwrapper.Wrapper,
	info *coretaskinfo.Info,
	payloads []byte,
) refs.Collection {
	if it.isExcludeErrReferenceWrap(info) {
		empty := refs.Empty()

		return empty.
			AddCollectionIf(
				errWrap.HasReferences(),
				errWrap.References()).ToNonPtr()
	}

	var lazyMap map[string]string
	lazyMap = info.LazyMap()

	collection := refs.New(
		constants.Capacity5 + len(lazyMap))

	if errWrap.HasReferences() {
		collection.AddCollection(errWrap.References())
	}

	collection.AddHashMap(lazyMap)
	collection.AddPayloadBytesIf(
		info.IsIncludePayloads() && hasPayload,
		payloads)

	return collection.ToNonPtr()
}

func (it newFriendlyErrorCreator) isExcludeErrReferenceWrap(
	info *coretaskinfo.Info,
) bool {
	return info.IsExcludeAdditionalErrorWrap()
}

func (it newFriendlyErrorCreator) WrapErrorWithDetailsPayloads(
	errWrap *errorwrapper.Wrapper,
	info *coretaskinfo.Info,
	payloads []byte,
) *errorwrapper.Wrapper {
	return it.WrapErrorWithDetails(
		true,
		errWrap,
		info,
		payloads)
}

func (it newFriendlyErrorCreator) WrapErrorWithDetailsNoPayloads(
	errWrap *errorwrapper.Wrapper,
	info *coretaskinfo.Info,
) *errorwrapper.Wrapper {
	return it.WrapErrorWithDetails(
		false,
		errWrap,
		info,
		nil)
}

func (it newFriendlyErrorCreator) WrapErrorWithDetails(
	hasPayload bool,
	errWrap *errorwrapper.Wrapper,
	info *coretaskinfo.Info,
	payloads []byte,
) *errorwrapper.Wrapper {
	if errWrap.IsEmpty() || info.IsExcludeAdditionalErrorWrap() {
		return nil
	}

	references := it.CompiledReferences(
		hasPayload,
		errWrap,
		info,
		payloads)

	return errorwrapper.NewMsgDisplayErrorReferencesPtr(
		defaultSkipInternal,
		errWrap.Type(),
		errWrap.ErrorString(),
		references.Items()...)
}

func (it newFriendlyErrorCreator) IncludePayloadsOnly(
	errWrap *errorwrapper.Wrapper,
	payloads []byte,
) *errorwrapper.Wrapper {
	if errWrap.IsEmpty() {
		return nil
	}

	return errWrap.
		ConcatNew().
		MsgRefOne(
			codestack.Skip2,
			"",
			"Payloads",
			corejson.BytesToPrettyString(payloads))
}

func (it newFriendlyErrorCreator) IncludePayloadsAnyOnly(
	errWrap *errorwrapper.Wrapper,
	payloadsAny interface{},
) *errorwrapper.Wrapper {
	if errWrap.IsEmpty() {
		return nil
	}

	return errWrap.
		ConcatNew().
		PayloadsAny(payloadsAny)
}
