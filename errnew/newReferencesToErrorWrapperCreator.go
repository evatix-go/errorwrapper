package errnew

import (
	"errors"
	"strings"

	"gitlab.com/evatix-go/core/coretaskinfo"
	"gitlab.com/evatix-go/errorwrapper"
	"gitlab.com/evatix-go/errorwrapper/errtype"
	"gitlab.com/evatix-go/errorwrapper/ref"
	"gitlab.com/evatix-go/errorwrapper/refs"
)

type newReferencesToErrorWrapperCreator struct{}

func (it newReferencesToErrorWrapperCreator) Error(
	errType errtype.Variation,
	err error,
	references ...ref.Value,
) *errorwrapper.Wrapper {
	if err == nil {
		return nil
	}

	return errorwrapper.NewMsgDisplayErrorReferencesPtr(
		defaultSkipInternal,
		errType,
		err.Error(),
		references...)
}

func (it newReferencesToErrorWrapperCreator) ErrorBytes(
	errType errtype.Variation,
	err error,
	referenceBytes []byte,
) *errorwrapper.Wrapper {
	if err == nil {
		return nil
	}

	referencesCollection := refs.
		Default().
		AddBytes("reference", referenceBytes)

	return errorwrapper.NewErrUsingAllParamsPtr(
		defaultSkipInternal,
		errType,
		true,
		err,
		referencesCollection)
}

func (it newReferencesToErrorWrapperCreator) MessageBytes(
	errType errtype.Variation,
	msg string,
	referenceBytes []byte,
) *errorwrapper.Wrapper {
	if msg == "" {
		return nil
	}

	referencesCollection := refs.
		Default().
		AddBytes("reference", referenceBytes)

	return errorwrapper.NewErrUsingAllParamsPtr(
		defaultSkipInternal,
		errType,
		true,
		errors.New(msg),
		referencesCollection)
}

func (it newReferencesToErrorWrapperCreator) MessageInfo(
	errType errtype.Variation,
	msg string,
	infoRef *coretaskinfo.Info,
) *errorwrapper.Wrapper {
	if msg == "" {
		return nil
	}

	referencesCollection := refs.
		Default().
		AddInfo(infoRef)

	return errorwrapper.NewErrUsingAllParamsPtr(
		defaultSkipInternal,
		errType,
		true,
		errors.New(msg),
		referencesCollection)
}

func (it newReferencesToErrorWrapperCreator) ErrorUsingStackSkip(
	stackSkipIndex int,
	errType errtype.Variation,
	err error,
	references ...ref.Value,
) *errorwrapper.Wrapper {
	if err == nil {
		return nil
	}

	return errorwrapper.NewMsgDisplayErrorReferencesPtr(
		stackSkipIndex+defaultSkipInternal,
		errType,
		err.Error(),
		references...)
}

func (it newReferencesToErrorWrapperCreator) ErrorWithMessage(
	errType errtype.Variation,
	err error,
	message string,
	references ...ref.Value,
) *errorwrapper.Wrapper {
	if err == nil {
		return nil
	}

	return errorwrapper.NewMsgDisplayErrorReferencesPtr(
		defaultSkipInternal,
		errType,
		errorwrapper.MessagesJoined(err.Error(), message),
		references...)
}

func (it newReferencesToErrorWrapperCreator) ErrorWithMessageUsingStackSkip(
	stackSkipIndex int,
	errType errtype.Variation,
	err error,
	message string,
	references ...ref.Value,
) *errorwrapper.Wrapper {
	if err == nil {
		return nil
	}

	return errorwrapper.NewMsgDisplayErrorReferencesPtr(
		stackSkipIndex+defaultSkipInternal,
		errType,
		errorwrapper.MessagesJoined(err.Error(), message),
		references...)
}

func (it newReferencesToErrorWrapperCreator) Msg(
	errType errtype.Variation,
	msg string,
	references ...ref.Value,
) *errorwrapper.Wrapper {
	if msg == "" {
		return nil
	}

	return errorwrapper.NewMsgDisplayErrorReferencesPtr(
		defaultSkipInternal,
		errType,
		msg,
		references...)
}

func (it newReferencesToErrorWrapperCreator) AddUsingFunc(
	errType errtype.Variation,
	err error,
	referencesAdderFunc func(refsCollection *refs.Collection) *refs.Collection,
) *errorwrapper.Wrapper {
	if err == nil {
		return nil
	}

	refsCollection := refs.New2()
	compiledReference := referencesAdderFunc(refsCollection)

	return errorwrapper.NewErrUsingAllParamsPtr(
		defaultSkipInternal,
		errType,
		true,
		err,
		compiledReference)
}

func (it newReferencesToErrorWrapperCreator) AddMsgUsingFunc(
	errType errtype.Variation,
	msg string,
	referencesAdderFunc func(refsCollection *refs.Collection) *refs.Collection,
) *errorwrapper.Wrapper {
	if msg == "" {
		return nil
	}

	refsCollection := refs.New2()
	compiledReference := referencesAdderFunc(refsCollection)

	return errorwrapper.NewErrUsingAllParamsPtr(
		defaultSkipInternal,
		errType,
		true,
		errors.New(msg),
		compiledReference)
}

func (it newReferencesToErrorWrapperCreator) MsgUsingStackSkip(
	stackSkipIndex int,
	errType errtype.Variation,
	message string,
	refValues ...ref.Value,
) *errorwrapper.Wrapper {
	reference := refs.
		New(len(refValues)).
		Adds(refValues...)

	return errorwrapper.NewMsgDisplayError(
		stackSkipIndex+defaultSkipInternal,
		errType,
		message,
		reference)
}

func (it newReferencesToErrorWrapperCreator) Type(
	errType errtype.Variation,
	refValues ...ref.Value,
) *errorwrapper.Wrapper {
	reference := refs.
		New(len(refValues)).
		Adds(refValues...)

	return errorwrapper.NewMsgDisplayError(
		defaultSkipInternal,
		errType,
		"",
		reference)
}

func (it newReferencesToErrorWrapperCreator) UsingStackSkip(
	stackSkipIndex int,
	errType errtype.Variation,
	refValues ...ref.Value,
) *errorwrapper.Wrapper {
	reference := refs.
		New(len(refValues)).
		Adds(refValues...)

	return errorwrapper.NewMsgDisplayError(
		stackSkipIndex+defaultSkipInternal,
		errType,
		"",
		reference)
}

func (it newReferencesToErrorWrapperCreator) Many(
	errType errtype.Variation,
	refValues ...ref.Value,
) *errorwrapper.Wrapper {
	reference := refs.
		New(len(refValues)).
		Adds(refValues...)

	return errorwrapper.NewMsgDisplayError(
		defaultSkipInternal,
		errType,
		"",
		reference)
}

func (it newReferencesToErrorWrapperCreator) ManyUsingStackSkip(
	stackSkipIndex int,
	errType errtype.Variation,
	refValues ...ref.Value,
) *errorwrapper.Wrapper {
	reference := refs.
		New(len(refValues)).
		Adds(refValues...)

	return errorwrapper.NewMsgDisplayError(
		stackSkipIndex+defaultSkipInternal,
		errType,
		"",
		reference)
}

func (it newReferencesToErrorWrapperCreator) ErrorWithOne(
	errType errtype.Variation,
	err error,
	reference ref.Value,
) *errorwrapper.Wrapper {
	if err == nil {
		return nil
	}

	return errorwrapper.NewMsgDisplayErrorReferencesPtr(
		defaultSkipInternal,
		errType,
		err.Error(),
		reference)
}

func (it newReferencesToErrorWrapperCreator) ErrorWithOneUsingStackSkip(
	stackSkipIndex int,
	errType errtype.Variation,
	err error,
	reference ref.Value,
) *errorwrapper.Wrapper {
	if err == nil {
		return nil
	}

	return errorwrapper.NewMsgDisplayErrorReferencesPtr(
		stackSkipIndex+defaultSkipInternal,
		errType,
		err.Error(),
		reference)
}

func (it newReferencesToErrorWrapperCreator) MsgWithOne(
	errType errtype.Variation,
	msg string,
	reference ref.Value,
) *errorwrapper.Wrapper {
	if msg == "" {
		return nil
	}

	return errorwrapper.NewMsgDisplayErrorReferencesPtr(
		defaultSkipInternal,
		errType,
		msg,
		reference)
}

func (it newReferencesToErrorWrapperCreator) MsgWithOneUsingStackSkip(
	stackSkipIndex int,
	errType errtype.Variation,
	msg string,
	reference ref.Value,
) *errorwrapper.Wrapper {
	if msg == "" {
		return nil
	}

	return errorwrapper.NewMsgDisplayErrorReferencesPtr(
		stackSkipIndex+defaultSkipInternal,
		errType,
		msg,
		reference)
}

func (it newReferencesToErrorWrapperCreator) OnlyOne(
	errType errtype.Variation,
	varName string,
	value interface{},
) *errorwrapper.Wrapper {
	return errorwrapper.NewRefOne(
		defaultSkipInternal,
		errType,
		varName,
		value)
}

func (it newReferencesToErrorWrapperCreator) OnlyOneUsingStackSkip(
	stackSkipIndex int,
	errType errtype.Variation,
	varName string,
	value interface{},
) *errorwrapper.Wrapper {
	return errorwrapper.NewRefOne(
		stackSkipIndex+defaultSkipInternal,
		errType,
		varName,
		value)
}

func (it newReferencesToErrorWrapperCreator) MergeWrapper(
	currentWrapper *errorwrapper.Wrapper,
	additionalReferences ...ref.Value,
) *errorwrapper.Wrapper {
	if currentWrapper == nil || currentWrapper.IsEmpty() {
		return nil
	}

	references := refs.
		NewExistingCollectionPlusAddition(
			currentWrapper.References(),
			additionalReferences...)

	return errorwrapper.NewMsgDisplayError(
		defaultSkipInternal,
		currentWrapper.Type(),
		currentWrapper.Error().Error(),
		references)
}

func (it newReferencesToErrorWrapperCreator) MergeWrapperUsingStackSkip(
	stackSkipIndex int,
	currentWrapper *errorwrapper.Wrapper,
	additionalReferences ...ref.Value,
) *errorwrapper.Wrapper {
	if currentWrapper == nil || currentWrapper.IsEmpty() {
		return nil
	}

	references := currentWrapper.MergeNewReferences(
		additionalReferences...)

	return errorwrapper.NewMsgDisplayError(
		stackSkipIndex+defaultSkipInternal,
		currentWrapper.Type(),
		currentWrapper.Error().Error(),
		references)
}

func (it newReferencesToErrorWrapperCreator) Messages(
	variant errtype.Variation,
	references *refs.Collection,
	messages ...string,
) *errorwrapper.Wrapper {
	compiledMessage := errorwrapper.MessagesJoined(messages...)

	return errorwrapper.NewMsgDisplayError(
		defaultSkipInternal,
		variant,
		compiledMessage,
		references)
}

func (it newReferencesToErrorWrapperCreator) ErrorMessages(
	variant errtype.Variation,
	err error,
	references *refs.Collection,
	messages ...string,
) *errorwrapper.Wrapper {
	if err == nil {
		return nil
	}

	compiledMessage := errorwrapper.MessagesJoined(
		messages...)

	return errorwrapper.NewMsgDisplayError(
		defaultSkipInternal,
		variant,
		compiledMessage,
		references)
}

func (it newReferencesToErrorWrapperCreator) ErrorMessagesUsingStackSkip(
	stackSkipIndex int,
	variant errtype.Variation,
	err error,
	references *refs.Collection,
	messages ...string,
) *errorwrapper.Wrapper {
	if err == nil {
		return nil
	}

	compiledMessage := errorwrapper.MessagesJoined(messages...)

	return errorwrapper.NewMsgDisplayError(
		stackSkipIndex+defaultSkipInternal,
		variant,
		compiledMessage,
		references)
}

func (it newReferencesToErrorWrapperCreator) MessagesUsingStackSkip(
	stackSkipIndex int,
	variant errtype.Variation,
	references *refs.Collection,
	messages ...string,
) *errorwrapper.Wrapper {
	compiledMessage := errorwrapper.MessagesJoined(messages...)

	return errorwrapper.NewMsgDisplayError(
		stackSkipIndex+defaultSkipInternal,
		variant,
		compiledMessage,
		references)
}

func (it newReferencesToErrorWrapperCreator) OneErrorMessages(
	stackSkipIndex int,
	variant errtype.Variation,
	err error,
	referenceOne ref.Value,
	messages ...string,
) *errorwrapper.Wrapper {
	if err == nil {
		return nil
	}

	compiledMessage := errorwrapper.MessagesJoined(
		messages...)

	return errorwrapper.NewMsgDisplayErrorReferencesPtr(
		stackSkipIndex+defaultSkipInternal,
		variant,
		compiledMessage,
		referenceOne)
}

func (it newReferencesToErrorWrapperCreator) OneErrorMessagesUsingStackSkip(
	stackSkipIndex int,
	variant errtype.Variation,
	err error,
	referenceOne ref.Value,
	messages ...string,
) *errorwrapper.Wrapper {
	if err == nil {
		return nil
	}

	compiledMessage := errorwrapper.MessagesJoined(
		messages...)

	return errorwrapper.NewMsgDisplayErrorReferencesPtr(
		stackSkipIndex+defaultSkipInternal,
		variant,
		compiledMessage,
		referenceOne)
}

func (it newReferencesToErrorWrapperCreator) MessagesUsingJoiner(
	variant errtype.Variation,
	references *refs.Collection,
	joiner string,
	messages ...string,
) *errorwrapper.Wrapper {
	compiledMessage := strings.Join(messages, joiner)

	return errorwrapper.NewMsgDisplayError(
		defaultSkipInternal,
		variant,
		compiledMessage,
		references)
}

func (it newReferencesToErrorWrapperCreator) MessagesUsingJoinerStackSkip(
	stackSkipIndex int,
	variant errtype.Variation,
	references *refs.Collection,
	joiner string,
	messages ...string,
) *errorwrapper.Wrapper {
	compiledMessage := strings.Join(messages, joiner)

	return errorwrapper.NewMsgDisplayError(
		stackSkipIndex+defaultSkipInternal,
		variant,
		compiledMessage,
		references)
}

// TypeQuick - errorTypeName - (...., items)...
func (it newReferencesToErrorWrapperCreator) TypeQuick(
	errType errtype.Variation,
	referencesValues ...interface{},
) *errorwrapper.Wrapper {
	return errorwrapper.TypeReferenceQuick(
		defaultSkipInternal,
		errType,
		referencesValues...,
	)
}

// TypeQuickUsingStackSkip - errorTypeName - (...., items)...
func (it newReferencesToErrorWrapperCreator) TypeQuickUsingStackSkip(
	stackSkipIndex int,
	errType errtype.Variation,
	referencesValues ...interface{},
) *errorwrapper.Wrapper {
	return errorwrapper.TypeReferenceQuick(
		stackSkipIndex+defaultSkipInternal,
		errType,
		referencesValues...,
	)
}
