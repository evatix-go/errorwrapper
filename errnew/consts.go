package errnew

import "gitlab.com/evatix-go/core/constants"

const (
	defaultSkipInternal = constants.One
	pathRefKeyword      = "Path"
)
